class KeycloakToken {
  late String accessToken;
  late int expiresIn;
  late int refreshExpiresIn;
  late String refreshToken;
  late String tokenType;
  late int notBeforePolicy;
  late String sessionState;
  late String scope;

  KeycloakToken(
      String accessToken,
      int expiresIn,
      int refreshExpiresIn,
      String refreshToken,
      String tokenType,
      int notBeforePolicy,
      String sessionState,
      String scope) {
    this.accessToken = accessToken;
    this.expiresIn = expiresIn;
    this.refreshExpiresIn = refreshExpiresIn;
    this.refreshToken = refreshToken;
    this.tokenType = tokenType;
    this.notBeforePolicy = notBeforePolicy;
    this.sessionState = sessionState;
    this.scope = scope;
  }

  factory KeycloakToken.fromJson(Map<String, dynamic> json) {
    KeycloakToken response = new KeycloakToken(
        json['access_token'],
        json['expires_in'],
        json['refresh_expires_in'],
        json['refresh_token'],
        json['token_type'],
        json['not-before-policy'],
        json['session_state'],
        json['scope']);
    return response;
  }
}
