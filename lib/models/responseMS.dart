class ResponseMS {
  late String message;
  late String? data;
  late String? error;
  late String? code;

  ResponseMS(String message, String? data, String? error, String? code) {
    this.message = message;
    this.data = data;
    this.error = error;
    this.code = code;
  }

  factory ResponseMS.fromJson(Map<String, dynamic> json) {
    ResponseMS response = new ResponseMS(
        json['message'], json['data'], json['error'], json['code']);
    return response;
  }
}
