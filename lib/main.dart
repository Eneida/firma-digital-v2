import 'package:firma_digital/models/keycloakToken.dart';
import 'package:firma_digital/models/responseMS.dart';
import 'package:firma_digital/pages/registerFlow.dart';
import 'package:firma_digital/pages/updateFlow.dart';
import 'package:firma_digital/pages/verifyFlow.dart';
import 'package:firma_digital/services/identityService.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Permission.camera.request();
  await Permission.microphone.request();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Identidad Digital',
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
      ),
      home: MyHomePage(title: 'Identidad Digital'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController cItxt = new TextEditingController();
  late Future<KeycloakToken> reqToken;
  late Future<ResponseMS> response;

  @override
  void initState() {
    permissionStatus();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Container(
          margin: EdgeInsets.all(MediaQuery.of(context).size.width * 0.1),
          child: Column(
            children: [
              TextFormField(
                controller: cItxt,
                decoration: InputDecoration(
                    border: UnderlineInputBorder(),
                    labelText: 'Ingrese su CI:'),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.8,
                height: MediaQuery.of(context).size.height * 0.08,
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * 0.08),
                child: ElevatedButton(
                    onPressed: cItxt.text.length > 3
                        ? () {
                            setState(() {
                              reqToken = IdentityService().getToken();
                              reqToken.then((value) {
                                response = IdentityService().verifyRegister(
                                    cItxt.text, value.accessToken);
                                response.then((resValue) {
                                  print(
                                      'resValue.message: ${resValue.message}');
                                  print('resValue.data: ${resValue.data}');
                                  print('resValue.error: ${resValue.error}');
                                  print('resValue.code: ${resValue.code}');
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          title: Text("Resultado:"),
                                          content: Column(
                                            children: [
                                              Text(resValue.message),
                                              Text(resValue.data ?? 'null'),
                                              Text(resValue.error ?? 'null'),
                                              Text(resValue.code ?? 'null')
                                            ],
                                          ),
                                          actions: [
                                            ElevatedButton(
                                              child: Text("Aceptart"),
                                              onPressed: () {
                                                Navigator.pop(context);
                                              },
                                            ),
                                          ],
                                        );
                                      });
                                });
                              });
                            });
                          }
                        : null,
                    child: Text(
                      "VERIFICAR REGISTRO",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: MediaQuery.of(context).size.height * 0.025),
                    )),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.8,
                height: MediaQuery.of(context).size.height * 0.08,
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * 0.04),
                child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        reqToken = IdentityService().getToken();
                        reqToken.then((value) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => RegisterFlow(
                                      "https://intersoftfintechservices.com/#/r/" +
                                          value.accessToken)));
                        });
                      });
                    },
                    child: Text(
                      "REGISTRARME",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: MediaQuery.of(context).size.height * 0.025),
                    )),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.8,
                height: MediaQuery.of(context).size.height * 0.08,
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * 0.04),
                child: ElevatedButton(
                    onPressed: cItxt.text.length > 3
                        ? () {
                            setState(() {
                              reqToken = IdentityService().getToken();
                              reqToken.then((value) {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => VerifyFlow(
                                            "https://intersoftfintechservices.com/#/v/" +
                                                cItxt.text +
                                                "/" +
                                                value.accessToken)));
                              });
                            });
                          }
                        : null,
                    child: Text(
                      "VERIFICAR IDENTIDAD",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: MediaQuery.of(context).size.height * 0.025),
                    )),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.8,
                height: MediaQuery.of(context).size.height * 0.08,
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * 0.04),
                child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        reqToken = IdentityService().getToken();
                        reqToken.then((value) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => UpdateFlow(
                                      "https://intersoftfintechservices.com/#/u/" +
                                          value.accessToken)));
                        });
                      });
                    },
                    child: Text(
                      "ACTUALIZAR DATOS",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: MediaQuery.of(context).size.height * 0.025),
                    )),
              )
            ],
          ),
        ));
  }

  Future<PermissionStatus> permissionStatus() async {
    var status = await Permission.camera.status;
    print('status:: $status');
    if (status.isGranted) {
      print('permiso consedido');
    } else if (status.isDenied) {
      print('perimso denegado');
      if (await Permission.camera.request().isDenied) {
        print('entra permiso');
        status = await Permission.camera.request();
        print('status 2: $status');
        setState(() {});
      }
    }

    return status;
  }
}
