import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class RegisterFlow extends StatefulWidget {
  final String url;
  RegisterFlow(this.url);
  @override
  State<StatefulWidget> createState() => _RegisterFlowState(url);
}

class _RegisterFlowState extends State<RegisterFlow> {
  late InAppWebViewController _webViewController;
  final String toUrl;
  _RegisterFlowState(this.toUrl);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Registro de identidad",
      home: Scaffold(
        appBar: AppBar(
          title: Text("Registro de identidad"),
        ),
        body: InAppWebView(
          initialUrlRequest: URLRequest(url: Uri.parse(toUrl)),
          initialOptions: InAppWebViewGroupOptions(
              ios: IOSInAppWebViewOptions(allowsInlineMediaPlayback: true),
              crossPlatform:
                  InAppWebViewOptions(mediaPlaybackRequiresUserGesture: false)),
          onReceivedServerTrustAuthRequest: (controller, challenge) async {
            return ServerTrustAuthResponse(
                action: ServerTrustAuthResponseAction.PROCEED);
          },
          onWebViewCreated: (InAppWebViewController controller) {
            this._webViewController = controller;
            this._webViewController.addJavaScriptHandler(
                handlerName: 'registerFlowFlutter',
                callback: (args) {
                  print(args[0]);
                });
          },
          androidOnPermissionRequest: (InAppWebViewController controller,
              String origin, List<String> resources) async {
            return PermissionRequestResponse(
                resources: resources,
                action: PermissionRequestResponseAction.GRANT);
          },
        ),
      ),
    );
  }
}
