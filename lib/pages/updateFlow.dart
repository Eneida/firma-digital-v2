import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class UpdateFlow extends StatefulWidget {
  final String url;
  UpdateFlow(this.url);
  @override
  State<StatefulWidget> createState() => _UpdateFlowState(url);
}

class _UpdateFlowState extends State<UpdateFlow> {
  late InAppWebViewController _webViewController;
  final String toUrl;
  _UpdateFlowState(this.toUrl);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Actualizar identidad",
      home: Scaffold(
        appBar: AppBar(
          title: Text("Actualizar identidad"),
        ),
        body: InAppWebView(
          initialUrlRequest: URLRequest(url: Uri.parse(toUrl)),
          initialOptions: InAppWebViewGroupOptions(
              ios: IOSInAppWebViewOptions(allowsInlineMediaPlayback: true),
              crossPlatform:
                  InAppWebViewOptions(mediaPlaybackRequiresUserGesture: false)),
          onReceivedServerTrustAuthRequest: (controller, challenge) async {
            return ServerTrustAuthResponse(
                action: ServerTrustAuthResponseAction.PROCEED);
          },
          onWebViewCreated: (InAppWebViewController controller) {
            this._webViewController = controller;
            this._webViewController.addJavaScriptHandler(
                handlerName: 'updateFlowFlutter',
                callback: (args) {
                  print(args[0]);
                });
          },
          androidOnPermissionRequest: (InAppWebViewController controller,
              String origin, List<String> resources) async {
            return PermissionRequestResponse(
                resources: resources,
                action: PermissionRequestResponseAction.GRANT);
          },
        ),
      ),
    );
  }
}
