import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class VerifyFlow extends StatefulWidget {
  final String url;
  VerifyFlow(this.url);
  @override
  State<StatefulWidget> createState() => _VerifyFlowState(url);
}

class _VerifyFlowState extends State<VerifyFlow> {
  late InAppWebViewController _webViewController;
  final String toUrl;
  _VerifyFlowState(this.toUrl);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Verificar identidad",
      home: Scaffold(
        appBar: AppBar(
          title: Text("Verificar identidad"),
        ),
        body: InAppWebView(
          initialUrlRequest: URLRequest(url: Uri.parse(toUrl)),
          initialOptions: InAppWebViewGroupOptions(
              ios: IOSInAppWebViewOptions(allowsInlineMediaPlayback: true),
              crossPlatform:
                  InAppWebViewOptions(mediaPlaybackRequiresUserGesture: false)),
          onReceivedServerTrustAuthRequest: (controller, challenge) async {
            return ServerTrustAuthResponse(
                action: ServerTrustAuthResponseAction.PROCEED);
          },
          onWebViewCreated: (InAppWebViewController controller) {
            this._webViewController = controller;
            this._webViewController.addJavaScriptHandler(
                handlerName: 'verifyFlowFlutter',
                callback: (args) {
                  print(args[0]);
                });
          },
          androidOnPermissionRequest: (InAppWebViewController controller,
              String origin, List<String> resources) async {
            return PermissionRequestResponse(
                resources: resources,
                action: PermissionRequestResponseAction.GRANT);
          },
        ),
      ),
    );
  }
}
