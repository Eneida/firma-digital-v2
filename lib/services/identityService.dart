import 'dart:convert';
import 'package:firma_digital/models/keycloakToken.dart';
import 'package:firma_digital/models/responseMS.dart';
import 'package:http/http.dart' as http;

class IdentityService {
  Future<KeycloakToken> getToken() async {
    print("trying....");
    final response = await http.post(
        Uri.parse(
            'https://intersoftfintechservices.com:8443/auth/realms/digital_identity/protocol/openid-connect/token'),
        headers: {"Content-Type": "application/x-www-form-urlencoded"},
        encoding: Encoding.getByName('utf-8'),
        body: {
          "client_id": "digital-identity",
          "username": "ernesto",
          "password": "cirrusitbanca",
          "grant_type": "password",
          "client_secret": "3caa2390-eeb5-4834-bfe9-0dd67ea89a61"
        });
    if (response.statusCode == 200) {
      print(jsonDecode(response.body));
      return KeycloakToken.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load Response TOKEN');
    }
  }

  Future<ResponseMS> verifyRegister(String ci, String token) async {
    final response = await http.get(
        Uri.parse(
            'https://intersoftfintechservices.com:8090/api/ms-digital-identity/verify-register/' +
                ci),
        headers: {'Authorization': 'Bearer $token'});
    if (response.statusCode == 200) {
      return ResponseMS.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode);
      throw Exception('Failed to load Response VERIFY');
    }
  }
}
